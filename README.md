2048
====

2048 game player 


Just open the game page(http://git.io/2048), open your console (F12), and add this code to your page to see the result :)



This code tries to solve the game by forcasting the best move.

'heuristic' function forcast the game as many times you define (var iteration)  and chose statistically the best move.

'forcast' function plays the game in depth you defaine (var depth) in all directions and compares the result of each direction and returns the move which has highest point.

'evaluate' function calculate a point for the given grid.

'play' function play the grid in given direction and returns the result grid and some more statistics may be needed for evaluating grid.

Feel free to change the evaluation algorithm, iteration and depth numbers or all other point numbers defined in the very begining of the code and of course notify me if you find a better way :)

Thanks to my dear friend RXDelta for helping me. enjoy it ;)
