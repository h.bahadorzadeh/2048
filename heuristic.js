var grid = new GameManager(4, KeyboardInputManager, HTMLActuator, LocalStorageManager);
var movePoint       = 1;
var valuePoint      = 1;
var depth           = 5;
var depthInertia    = 9/10;
var iteration       = 50;
var orderingPoint   = 2;
//var directionsPoint = [10, 10, 10 , 1, 1];

var cloneArray = function (array){
	var clone = [[null, null, null, null],
		  [null, null, null, null],
	          [null, null, null, null],
	          [null, null, null, null]];
	for (var i in array){
		for (var j in array[i]){
			clone[i][j]=array[i][j];
		}
	}
	return clone;
}

var getFreeCells = function (grid){
	var freeCells = new Array();
	for (var i in grid){
		for (var j in grid[i]){
			if(grid[i][j]==null){
				freeCells.push([i,j]);
			}
		}
	}
	return freeCells;
}

var getGridMax = function(grid){
	maxVal = [0,0,0];
	for (var i in grid){
		for (var j in grid[i]){
			if(grid[i][j] && grid[i][j]> maxVal[0]){maxVal = [grid[i][j],i,j];};
		}
	}
	return maxVal;
}

var addRandomTile = function(grid){
	freeCells = getFreeCells(grid);
	if(freeCells.length == 0){
		return [0, 0, 0, -1];
	}
	var newTile = freeCells[Math.floor(Math.random() * freeCells.length)];
	grid[newTile[0]][newTile[1]] = Math.random() < 0.9 ? 2 : 4;
	return [freeCells.length, newTile[0], newTile[1], grid[newTile[0]][newTile[1]]];
}

var evaluate = function (result){
//result = [playGrid, moves, sumMoves, sumValue, freeCells];	
//directionsPoint
        var playGrid  = result[0],
	    moves     = result[1],
            sumMoves  = result[2],
            sumValue  = result[3],
            freeCells = result[4];
	var maxValue  = getGridMax(playGrid);
	var maxValueI = maxValue[1];
	var maxValueJ = maxValue[2];
	var maxValue  = maxValue[0];
//giving grid a value
	var gridValue = 0;
 	freeCells+= 1/2;
	if(maxValueI > 1){
		var initI = 0,finishI = 4, plusI = 1;
	}else{
		var initI = 3,finishI = -1, plusI = -1;	
	}

	if(maxValueJ > 1){
		var initJ = 0,finishJ = 4, plusJ = 1;
	}else{
		var initJ = 3,finishJ = -1, plusJ = -1;	
	}
	var iCounter = 0;
	for(var i=initI; i!= finishI; i+=plusI){
		var jCounter = ((i%2)==0)? 4 : 0;
		for(var j=initJ; j!= finishJ; j+=plusJ){
			if(playGrid[j][i]) gridValue+=(playGrid[j][i]*Math.pow(orderingPoint, iCounter)*Math.pow(orderingPoint, jCounter));
			if((i%2)==0){
				jCounter++;
			}else{
				jCounter--;
			}
		}
		iCounter++;
	}
        var value = gridValue*(Math.log(freeCells)/Math.log(2));
	return isNaN(value) ? 0 : value;
}

var play = function(direction, playGrid) {
// 0 = up, 1= right, 2 = down, 3 = left
  var init,
      finish,
      plus;
  var ij; //0=vertical;1=horizental
  switch (direction) {
    case 0: //up
      init=0;finish=4;plus=1;
      ij=0;
      break;
    case 1: //right
      init=3;finish=-1;plus=-1;
      ij=1;
      break;
    case 2: //down
      init=3;finish=-1;plus=-1;
      ij=0;
      break;
    case 3: //left
      init=0;finish=4;plus=1;
      ij=1;
      break;
  }
  var moves    = 0;
  var sumMoves = 0;
  var sumValue = 0;
  for (var i=0;i<4;i++) {
    var previus = [i, init, (ij==0) ? playGrid[init][i] : playGrid[i][init]];
    var start = init+plus;
    for (var j=start;j!=finish; j+= plus) {
        var value = (ij==0) ? playGrid[j][i] : playGrid[i][j];
        if(value != null){
		if(previus[2] != null){
			if(previus[2] == value){
				moves++;
                                sumMoves++;
                                sumValue += value;
				if(ij==0){
					playGrid[previus[1]][previus[0]] = value*2;
					playGrid[j][i]=null;
				}else{
					playGrid[previus[0]][previus[1]] = value*2;
					playGrid[i][j]=null;
				}
				previus = [i, j, null];
			}else{
				if( j!= previus[1]+plus ){
					moves++;
					if(ij==0){
						playGrid[previus[1]+plus][i] = value;
						playGrid[j][i]=null;
					}else{
						playGrid[i][previus[1]+plus] = value;
						playGrid[i][j]=null;
					}
					previus = [i, j-plus, value];
				}else{
					previus = [i, j, value];
				}
			}
		}else{
			moves++;
			if(ij==0){
				playGrid[previus[1]][previus[0]] = value;
				playGrid[j][i]=null;
			}else{
				playGrid[previus[0]][previus[1]] = value;
				playGrid[i][j]=null;
			}
			previus [2] = value;
		}
	}	
    }
  }
  var freeCells = addRandomTile(playGrid)[0];
  return [playGrid, moves, sumMoves, sumValue, freeCells];	
}

var getPlayGrid = function(){
	  var playGrid = [[null, null, null, null],
		  [null, null, null, null],
	          [null, null, null, null],
	          [null, null, null, null]];
	  for( var k in grid.grid.cells){
	  	for( var j in grid.grid.cells[k]){
			if( grid.grid.cells[j][k] ){
				playGrid[k][j] = grid.grid.cells[j][k].value;
			}
		}
	  }
	return playGrid;
}

var forcast = function (playGrid,depth){
	var result = [-1, evaluate([playGrid, 0, 0, 0, 0])];
	var tmpGridValue = 0;
	for(var i=0; i<=3 ; i++){
		var tmpPlayGrid = cloneArray(playGrid);
		var tmpResult = play(i ,tmpPlayGrid);
		if( tmpResult[1] == 0 ) continue;
		tmpGridValue  = evaluate(tmpResult);
		if( result[1] < tmpGridValue ){
			result = [i, tmpGridValue];
		}
		if(depth > 1){
			var tmpDepthResult=forcast(tmpResult[0], depth-1);
			tmpGridValue = tmpDepthResult[1] * depthInertia;
		}
		if( result[1] < tmpGridValue ){
			result = [i, tmpGridValue];
		}
	}
	return result;
}

var maxIndex = function(arr) {
  var result=-1;
  for (var i in arr) {
     if (arr[i] != 0 && (result == -1 || arr[result] < arr[i])) {
        result = i;
     }
  }
  return result;
} 

var heuristic = function() {
  var playGrid = getPlayGrid();
  // up-right-down-left
  var count  = [0, 0, 0, 0, 0];
  for (var i=0;i<iteration;i++) {
      var result = forcast(playGrid, depth);
      if( 0<=result[0] && result[0]<=3 ){
	 count[result[0]]++;
      }else{
	 count[4]++;
      }
  }
  var dir = maxIndex(count);
  if (dir == 4) dir = Math.round(Math.random()*3);
  grid.move(dir);
  return dir;
}
setInterval(heuristic,100);
